package ua.heryv1m.elastictest.utils;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class ValidationUtils {

    public static String validationMessage(BindingResult br) {
        StringBuilder stringBuilder = new StringBuilder();
        for(FieldError fe : br.getFieldErrors()) {
            stringBuilder.append("Field ")
                    .append("\"")
                    .append(fe.getField())
                    .append("\": ")
                    .append(fe.getDefaultMessage())
                    .append(";");
        }

        return stringBuilder.toString();
    }
}
