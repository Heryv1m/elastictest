package ua.heryv1m.elastictest.utils.type;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum DnbResultID {
    CM000,
    SC001;

    @JsonCreator
    public static DnbResultID forValue(String value) {
        return DnbResultID.valueOf(value);
    }
}
