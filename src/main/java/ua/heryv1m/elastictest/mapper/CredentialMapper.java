package ua.heryv1m.elastictest.mapper;

import ua.heryv1m.elastictest.dto.auth.CredentialDto;
import ua.heryv1m.elastictest.entity.Credential;

public class CredentialMapper {

    public static Credential toEntity(CredentialDto dto) {
        return new Credential(dto.getUserId(), dto.getPassword());
    }
}
