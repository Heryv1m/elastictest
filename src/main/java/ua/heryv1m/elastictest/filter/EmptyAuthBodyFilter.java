package ua.heryv1m.elastictest.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(0)
@Component
public class EmptyAuthBodyFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request.getContentLength() == 0) {
            chain.doFilter(request, response);
        } else {
            String message = "{\n\"code\": \"Bad request\",\n\"message\": \"Missing request body\"\n}";
            HttpServletResponse resp = (HttpServletResponse) response;
            resp.getWriter().write(message);
            resp.setStatus(400);
        }
    }
}
