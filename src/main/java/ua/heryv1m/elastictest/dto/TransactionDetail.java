package ua.heryv1m.elastictest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionDetail {

    @JsonProperty("TransactionDetail")
    private String srvTrxID;

    @JsonProperty("TransactionTimestamp")
    private String trxDate;

    @JsonProperty("ApplicationTransactionID")
    private String appTrxID;
}
