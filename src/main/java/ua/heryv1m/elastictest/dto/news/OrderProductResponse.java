package ua.heryv1m.elastictest.dto.news;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.heryv1m.elastictest.dto.TransactionDetail;
import ua.heryv1m.elastictest.dto.TransactionResult;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderProductResponse {

    @JsonProperty("TransactionalDetail")
    private TransactionDetail trxDetail;
    @JsonProperty("TransactionalResult")
    private TransactionResult trxResult;
    @JsonProperty("OrderProductResponseDetail")
    private OrderProductResponseDetail oprd;
}
