package ua.heryv1m.elastictest.dto.news;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsDetail {

    @JsonProperty("PublishedDateTimestamp")
    private LocalDateTime dateTime;
    @JsonProperty("SourceDescription")
    private String sourceDescription;
    @JsonProperty("CategoryText")
    private List<CategoryText> textCategory;
    @JsonProperty("TitleText")
    private String titleText;
    @JsonProperty("ContentText")
    private String contentText;
    @JsonProperty("WebPageURL")
    private String webPageUrl;
}
