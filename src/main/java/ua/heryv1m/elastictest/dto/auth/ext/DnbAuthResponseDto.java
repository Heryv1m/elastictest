package ua.heryv1m.elastictest.dto.auth.ext;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ua.heryv1m.elastictest.dto.TransactionDetail;
import ua.heryv1m.elastictest.dto.TransactionResult;

@Data
public class DnbAuthResponseDto {

    @JsonProperty("TransactionDetail")
    private TransactionDetail trxDetail;

    @JsonProperty("TransactionResult")
    private TransactionResult trxResult;

    @JsonProperty("AuthenticationDetail")
    private AuthenticationDetail detail;
}
