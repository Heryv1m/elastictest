package ua.heryv1m.elastictest.dto.auth;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CredentialDto {

    @NotBlank
    private String userId;
    @NotBlank
    private String password;
}
