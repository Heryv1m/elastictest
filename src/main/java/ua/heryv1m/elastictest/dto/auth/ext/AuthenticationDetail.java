package ua.heryv1m.elastictest.dto.auth.ext;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AuthenticationDetail {

    @JsonProperty("Token")
    private String token;
}
