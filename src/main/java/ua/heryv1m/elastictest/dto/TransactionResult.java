package ua.heryv1m.elastictest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ua.heryv1m.elastictest.utils.type.DnbResultID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionResult {

    @JsonProperty("SeverityText")
    private String severityText;

    @JsonProperty("ResultID")
    private DnbResultID resultId;

    @JsonProperty("ResultText")
    private String resultText;
}
