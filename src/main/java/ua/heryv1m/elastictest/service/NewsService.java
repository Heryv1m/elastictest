package ua.heryv1m.elastictest.service;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ua.heryv1m.elastictest.dto.news.NewsDetails;
import ua.heryv1m.elastictest.dto.news.NewsDto;
import ua.heryv1m.elastictest.dto.news.OrderProductResponse;
import ua.heryv1m.elastictest.dto.news.OrderProductResponseDetail;

@Service
public class NewsService {

    private final RestTemplate restTemplate;

    @Value("${dnb.url.news}")
    private String extAuthUrl;

    @Autowired
    public NewsService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public NewsDetails getCompanyNews(String token, int dunsNumber) {
        String url = extAuthUrl.replace("$1", String.valueOf(dunsNumber));
        RequestEntity req = RequestEntity.get(URI.create(url))
                .header("Authorization", token)
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .build();

        ResponseEntity<NewsDto> exchange = restTemplate.exchange(req, NewsDto.class);

        if (exchange.getStatusCode().is2xxSuccessful()) {
            OrderProductResponseDetail oprd = exchange.getBody().getProductResponse().getOprd();
            if (oprd.getProduct().getOrganization().getNews() != null) {
                return oprd.getProduct().getOrganization().getNews();
            }
        }

        return new NewsDetails();
    }
}
