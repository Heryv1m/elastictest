package ua.heryv1m.elastictest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ua.heryv1m.elastictest.dto.auth.AuthResponseDto;
import ua.heryv1m.elastictest.dto.auth.ext.DnbAuthResponseDto;
import ua.heryv1m.elastictest.entity.Credential;

@Service
public class AuthorizationService {

    private final RestTemplate restTemplate;

    @Value("${dnb.url.auth}")
    private String extAuthUrl;

    @Autowired
    public AuthorizationService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public AuthResponseDto authorize(Credential credential) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("x-dnb-user", credential.getUserId());
        headers.add("x-dnb-pwd", credential.getPassword());

        HttpEntity req = new HttpEntity(headers);
        try {
            DnbAuthResponseDto authInfo = restTemplate.postForEntity(extAuthUrl, req, DnbAuthResponseDto.class)
                    .getBody();
            return new AuthResponseDto(authInfo.getDetail().getToken(), "OK", authInfo.getTrxResult().getResultText());
        } catch (Exception e) {
            return new AuthResponseDto(null, "Unauthorized", "Invalid Credentials");
        }
    }
}
