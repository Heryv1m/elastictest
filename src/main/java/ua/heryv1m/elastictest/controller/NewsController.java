package ua.heryv1m.elastictest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.heryv1m.elastictest.service.NewsService;

@RestController
@RequestMapping("news")
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping("{dunsNum}")
    public ResponseEntity<?> getNews(@RequestHeader("token") String token,
                                     @PathVariable("dunsNum") Integer dunsNum) {

        return ResponseEntity.ok(newsService.getCompanyNews(token, dunsNum));
    }
}
