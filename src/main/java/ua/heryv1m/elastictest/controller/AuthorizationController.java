package ua.heryv1m.elastictest.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.heryv1m.elastictest.dto.auth.CredentialDto;
import ua.heryv1m.elastictest.dto.auth.AuthResponseDto;
import ua.heryv1m.elastictest.mapper.CredentialMapper;
import ua.heryv1m.elastictest.service.AuthorizationService;
import ua.heryv1m.elastictest.utils.ValidationUtils;

@RestController
@RequestMapping("auth")
public class AuthorizationController {

    private final AuthorizationService authService;

    @Autowired
    public AuthorizationController(AuthorizationService authService) {
        this.authService = authService;
    }

    @PostMapping(value = "login", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> authorize(@RequestBody @Valid CredentialDto credential, BindingResult br) {
        if (br.hasErrors()) {
            return ResponseEntity.badRequest()
                    .body(new AuthResponseDto(null, "Bad request", ValidationUtils.validationMessage(br)));
        } else {
            AuthResponseDto dto = authService.authorize(CredentialMapper.toEntity(credential));
            return ResponseEntity.ok(dto);
        }
    }
}
