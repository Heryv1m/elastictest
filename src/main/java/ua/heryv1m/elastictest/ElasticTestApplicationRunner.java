package ua.heryv1m.elastictest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "ua.heryv1m.elastictest")
public class ElasticTestApplicationRunner {

    public static void main(String[] args) {
        SpringApplication.run(ElasticTestApplicationRunner.class, args);
    }
}
