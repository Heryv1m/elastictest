package ua.heryv1m.elastictest.config;

import javax.validation.Validation;
import javax.validation.Validator;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ua.heryv1m.elastictest.filter.EmptyAuthBodyFilter;

@EnableWebMvc
@Configuration
public class AppConfig {

    @Bean
    public FilterRegistrationBean<EmptyAuthBodyFilter> loggingFilter(){
        FilterRegistrationBean<EmptyAuthBodyFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new EmptyAuthBodyFilter());
        registrationBean.addUrlPatterns("/auth/login");

        return registrationBean;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public Validator validator() {
        return Validation.buildDefaultValidatorFactory().getValidator();
    }
}
