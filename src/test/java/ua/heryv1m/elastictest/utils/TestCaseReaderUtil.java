package ua.heryv1m.elastictest.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TestCaseReaderUtil {

    public static String readJsonFromFile(String pathToFile) throws IOException {
        File file = new File(ClassLoader.getSystemResource(pathToFile).getFile());
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        StringBuilder builder = new StringBuilder();
        while (bufferedReader.ready()) {
            builder.append(bufferedReader.readLine());
        }

        return builder.toString();
    }
}
