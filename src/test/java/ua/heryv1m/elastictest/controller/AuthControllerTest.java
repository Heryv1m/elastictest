package ua.heryv1m.elastictest.controller;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import ua.heryv1m.elastictest.dto.auth.AuthResponseDto;
import ua.heryv1m.elastictest.utils.TestCaseReaderUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

class AuthControllerTest extends BaseControllerTest {

    private static final String V_REQUEST_JSON = "json/auth/v_request.json";
    private static final String NV_EMPTY_PASS_JSON = "json/auth/nv_empty_password.json";
    private static final String NV_EMPTY_USR_JSON = "json/auth/nv_empty_username.json";
    private static final String NV_NOT_EXISTS_PASS_JSON = "json/auth/nv_not_exists_password.json";
    private static final String NV_NOT_EXISTS_USR_JSON = "json/auth/nv_not_exists_username.json";
    private static final String NV_WRONG_CRED_JSON = "json/auth/nv_wrong_cred.json";

    @Test
    void successfulAuthRequestTest() throws Exception {
        String requestJson = TestCaseReaderUtil.readJsonFromFile(V_REQUEST_JSON);
        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(result -> {
                    AuthResponseDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDto.class);

                    assertEquals("OK", dto.getCode());
                    assertNotNull(dto.getToken());
                });
    }

    @Test
    void failAuth_CauseWrongCred() throws Exception {
        String requestJson = TestCaseReaderUtil.readJsonFromFile(NV_WRONG_CRED_JSON);
        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(result -> {
                    AuthResponseDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDto.class);

                    assertEquals("Unauthorized", dto.getCode());
                    assertEquals("Invalid Credentials", dto.getMessage());
                    assertNull(dto.getToken());
                });
    }

    @Test
    void failAuth_CauseEmptyUsername() throws Exception {
        String requestJson = TestCaseReaderUtil.readJsonFromFile(NV_EMPTY_USR_JSON);
        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(result -> {
                    AuthResponseDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDto.class);

                    assertEquals("Bad request", dto.getCode());
                    assertEquals("Field \"userId\": must not be blank;", dto.getMessage());
                    assertNull(dto.getToken());
                });
    }

    @Test
    void failAuth_CauseEmptyPassword() throws Exception {
        String requestJson = TestCaseReaderUtil.readJsonFromFile(NV_EMPTY_PASS_JSON);
        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(result -> {
                    AuthResponseDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDto.class);

                    assertEquals("Bad request", dto.getCode());
                    assertEquals("Field \"password\": must not be blank;", dto.getMessage());
                    assertNull(dto.getToken());
                });
    }

    @Test
    void failAuth_CauseNotExistsUsername() throws Exception {
        String requestJson = TestCaseReaderUtil.readJsonFromFile(NV_NOT_EXISTS_USR_JSON);
        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(result -> {
                    AuthResponseDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDto.class);

                    assertEquals("Bad request", dto.getCode());
                    assertEquals("Field \"userId\": must not be blank;", dto.getMessage());
                    assertNull(dto.getToken());
                });
    }

    @Test
    void failAuth_CauseNotExistsPassword() throws Exception {
        String requestJson = TestCaseReaderUtil.readJsonFromFile(NV_NOT_EXISTS_PASS_JSON);
        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(result -> {
                    AuthResponseDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDto.class);

                    assertEquals("Bad request", dto.getCode());
                    assertEquals("Field \"password\": must not be blank;", dto.getMessage());
                    assertNull(dto.getToken());
                });
    }

    @Test
    void failAuth_CauseRequestBodyIsEmpty() throws Exception {
        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(result -> {
                    AuthResponseDto dto = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDto.class);

                    assertEquals("Bad request", dto.getCode());
                    assertEquals("Missing request body", dto.getMessage());
                    assertNull(dto.getToken());
                });
    }
}
