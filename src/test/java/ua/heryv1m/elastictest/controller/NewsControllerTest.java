package ua.heryv1m.elastictest.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import ua.heryv1m.elastictest.dto.news.NewsDetails;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

class NewsControllerTest extends BaseControllerTest {

    /**
     * There need good @see DUNS_NUM variable
     * @arg DUMS_NUM
     */
    private static final int DUNS_NUM = 804735132;
    @Value("${dnb.auth.token}")
    private String AUTH_TOKEN;

    @Test
    void testSuccessfulGetNews() throws Exception {
        mvc.perform(get("/news/" + DUNS_NUM)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("token", AUTH_TOKEN))
                .andDo(print())
                .andExpect(result -> {
                    NewsDetails details = objectMapper.readValue(result.getResponse().getContentAsString(), NewsDetails.class);

                    assertNotNull(details.getNewsDetails());
                    assertFalse(details.getNewsDetails().isEmpty());
                });
    }

    @Test
    void testSuccessfulGetEmptyNews() throws Exception {
        mvc.perform(get("/news/" + DUNS_NUM)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("token", AUTH_TOKEN))
                .andExpect(result -> {
                    NewsDetails details = objectMapper.readValue(result.getResponse().getContentAsString(), NewsDetails.class);

                    assertNull(details.getNewsDetails());
                });
    }
}
