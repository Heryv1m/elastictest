package ua.heryv1m.elastictest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ua.heryv1m.elastictest.ElasticTestApplicationRunner;
import ua.heryv1m.elastictest.filter.EmptyAuthBodyFilter;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = ElasticTestApplicationRunner.class)
@AutoConfigureMockMvc
public abstract class BaseControllerTest {

    @Autowired
    protected WebApplicationContext context;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected MockMvc mvc;

    @BeforeEach
    protected void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilter(new EmptyAuthBodyFilter(), "/auth/login")
                .build();
    }
}